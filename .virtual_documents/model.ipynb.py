"""Importing the Data Science Libraries"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 
import seaborn as sns
import sklearn
from scipy.stats import chi2_contingency

from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split

import pickle

get_ipython().run_line_magic("matplotlib", " inline")


# Setting plot style 
sns.set_theme(context='poster', style='darkgrid', palette='Spectral', rc={'figure.figsize': (18, 8)})


X_train = pd.read_csv('data/training_data/X_train.csv')
X_test = pd.read_csv('data/testing_data/X_test.csv')
y_train = pd.read_csv('data/training_data/y_train.csv')
y_test = pd.read_csv('data/testing_data/y_test.csv')


X_train_shrinked = X_train.iloc[:8000, :]
y_train_shrinked = y_train.iloc[:8000]
X_test_shrinked = X_test.iloc[8000:10000, :]
y_test_shrinked = y_test.iloc[8000:10000]


X_train_shrinked.to_csv('data/training_data/shrinked_X_train.csv', index=False)
X_test_shrinked.to_csv("data/testing_data/shrinked_X_test.csv", index=False)
y_train_shrinked.to_csv('data/training_data/shrinked_y_train.csv', index=False)
y_test_shrinked.to_csv('data/testing_data/shrinked_y_test.csv', index=False)


X_train = X_train_shrinked
X_test = X_test_shrinked
y_train = y_train_shrinked
y_test = y_test_shrinked


print("X_train: ", X_train.shape)
print("y_train: ", y_train.shape)
print("X_test: ", X_test.shape)
print("y_test: ", y_test.shape)


# Converting label columns into pandas series 
y_train = y_train['BP']
y_test = y_test['BP']


print("y_train type: ", type(y_train))
print("y_test type: ", type(y_test))


# Linear Regression 
from sklearn.linear_model import LinearRegression

lin_reg = LinearRegression()
lin_reg.fit(X_train.values, y_train)


# Predicting bench press values given the model trained from the training data
y_pred = lin_reg.predict(X_test)


# compare the errors to guage accuracy
# for linear regression, we will use square root of the mean squared error (RMSE)
# lower the rmse, the more accurate the model 
from sklearn.metrics import mean_squared_error

mse_lin = mean_squared_error(y_test, y_pred)
rmse = np.sqrt(mse_lin)
print(f"Linear Regression RMSE: {mse_lin}")

# The rmse score is atrocious! This highlights that our data doesn't have a linear shape


# lets try a decision tree classifier
from sklearn.tree import DecisionTreeRegressor
tree_reg = DecisionTreeRegressor()
tree_reg.fit(X_train, y_train)
y_pred = tree_reg.predict(X_test)


tree_mse = mean_squared_error(y_test, y_pred)


tree_rmse = np.sqrt(tree_mse)
print(f"Decision Tree RMSE: {tree_rmse}")

# interesting! The tree mse score is much lower than linear regression mse score


# cross validation (hides testing data and discourages overfitting)
from sklearn.model_selection import cross_val_score 

scores = cross_val_score(tree_reg, X_train, y_train, cv=10, scoring='neg_mean_squared_error')


# WHY IS THE SCORES IN CROSSVAL LOWER THAN SCORE ON TESTING SET
scores


scores = -scores
tree_rmse_crossval = np.sqrt(scores.mean())
print(f'Decision Tree RMSE (CrossVal): {tree_rmse_crossval}')

# Decision Tree Model is performing unsually well on the training set


lin_scores = -cross_val_score(lin_reg, X_train, y_train, cv=10, scoring='neg_mean_squared_error')
lin_rmse_crossval = np.sqrt(lin_scores.mean())
lin_rmse_crossval

# crossval scores are significantly lower than testing data scores. This might be a problem 


# random forest
from sklearn.ensemble import RandomForestRegressor

forest_reg = RandomForestRegressor()
forest_reg.fit(X_train.values, y_train)
y_pred = forest_reg.predict(X_test)


# Cross validation for forest_Reg
def run_cross_val(model, X_train, y_train):
    """Runs cross validation on the given model and dataset"""
    scores = -cross_val_score(model, X_train, y_train, scoring='neg_root_mean_squared_error', cv=10)
    return scores, scores.mean()


forest_cross_val = run_cross_val(forest_reg, X_train.values, y_train.values)


forest_cross_val


from sklearn.svm import SVR

svm = SVR(kernel='linear')
svm.fit(X_train.values, y_train.values)
svm_cross_val = run_cross_val(svm, X_train.values, y_train.values)

# until now, random forest regressor has performed the best! 


svm_cross_val


# fine tuning hyper parameter optimization 


from sklearn.model_selection import GridSearchCV

param_grid = [
    {'n_estimators': [3, 10, 25], 'max_features': [2, 4, 6, 8]},
    {'n_estimators': [3, 10], "bootstrap": [False], 'max_features': [3, 2, 5, 7]}
]

forest_reg = RandomForestRegressor() 

grid_search = GridSearchCV(forest_reg, param_grid, scoring='neg_mean_squared_error', return_train_score=True, cv=10)

grid_search.fit(X_train.values, y_train.values)


best_params_forest = grid_search.best_params_


best_params_forest


cv_scores = grid_search.cv_results_


for score, param in zip(cv_scores['mean_test_score'], cv_scores["params"]):
    print(np.sqrt(-score), param)


# Let's run cross val CV again but with a modified estimator
def gridCrossValForest(param_grid, X, y):
    
    forest_reg = RandomForestRegressor() 
    grid_search = GridSearchCV(forest_reg, param_grid, scoring='neg_mean_squared_error', return_train_score=True, cv=10)
    grid_search.fit(X, y)
    
    # Best paramater combination
    cv_scores = grid_search.cv_results_
    
    for score, param in zip(cv_scores['mean_test_score'], cv_scores["params"]):
        print(np.sqrt(-score), param)
    
    return grid_search, grid_search.best_params_


param_grid = [
    {'n_estimators': [30, 35, 40], 'max_features': [2, 8]},
]

gridCrossValForest(param_grid, X_train.values, y_train.values)


# GridSearchCV number 3
param_grid = [
    {'n_estimators': [100], 'max_features': [8, 12]},
]

gridCrossValForest(param_grid, X_train.values, y_train.values)


feature_importances = grid_search.best_estimator_.feature_importances_
feature_importance_list = list(zip(X_train.columns, feature_importances))


feature_importance_list.sort(key = lambda x: x[1], reverse=True)


feature_importance_list


X_train.drop(['Tested', 'MeetCountry', 'AgeClass', 'Division'], axis=1, inplace=True)


X_test.drop(['Tested', 'MeetCountry', 'AgeClass', 'Division'], axis=1, inplace=True)


# running our model_again
param_grid = [
    {'n_estimators': [100], 'max_features': [8, 12]},
]

grid_search, best_params = gridCrossValForest(param_grid, X_train.values, y_train.values)


def checkFeatureImportance(grid_search, X):
    feature_importances = grid_search.best_estimator_.feature_importances_
    feature_importance_list = list(zip(X.columns, feature_importances))
    return feature_importance_list


checkFeatureImportance(grid_search, X_train)


# Dropping more columns 
X_train.drop(['MeetName', 'MeetState', 'Date', 'Federation', 'Place'], axis=1, inplace=True)
X_test.drop(['MeetName', 'MeetState', 'Date', 'Federation', 'Place'], axis=1, inplace=True)


X_train.shape


param_grid = [
    {'n_estimators': [200], 'max_features': [8]},
]

grid_search, best_params = gridCrossValForest(param_grid, X_train.values, y_train.values)
best_params


X_train.columns


X_train['Sex'].value_counts()


sns.countplot(x='Sex', data=X_train)


X_train['Sex'] = X_train.loc[:, 'Sex'].round(2)


# Setting males to 0 
# X_train.loc[X_train['Sex'] == 0.33, 'Sex'] = 0

# Setting females to 1
X_train.loc[X_train['Sex'] == -3.07, 'Sex'] = 1


X_train['Sex'] = X_train['Sex'].astype(int)


X_train['Sex'].value_counts()


# look at the sample of 10000 that you took
# explore the sample statistical attributes, see if the central stats match or not! 
# make sure sample is representitive of the entire dataset
# cross validation is better

# different metrics are for different situations 
# each metric has different 
# explore the different error metrics (what the accuracy means)








