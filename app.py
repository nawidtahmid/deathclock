from flask import Flask, render_template, request, redirect, url_for

# Create an instance of flask with the file name (__name__)
app = Flask(__name__)

@app.route('/<name>')
def hello_world(name):
    return f"<h2>Hello {name}</h2>"

# view function
@app.route('/bruv/<name>', methods=['GET', 'POST'])
def index(name):
    request_method = request.method
    return render_template('index.html', name=name , request_method=request_method)

@app.route("/", methods=["GET", "POST"])
def index2():
    request_method = request.method

    if request.method == "POST":
       print(request.form)
    #    return redirect(url_for('name'))
    
    return render_template('index.html', request_method=request_method)

@app.route('/name')
def name(first_name, last_name, request_method):
    return render_template('form_submission.html', first_name=first_name, last_name=last_name,
        request_method=request_method)


if __name__ == "__main__":
    app.run(debug=True)

